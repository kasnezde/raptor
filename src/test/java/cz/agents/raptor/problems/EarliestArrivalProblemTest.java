package cz.agents.raptor.problems;

import cz.agents.raptor.Raptor;
import cz.agents.raptor.structures.Journey;
import cz.agents.raptor.structures.Stop;
import cz.agents.raptor.structures.Timetable;
import cz.agents.raptor.structures.Trip;
import cz.agents.raptor.tools.TimeHelper;
import cz.agents.raptor.load.gtfs.Gtfs;
import cz.agents.raptor.load.gtfs.GtfsDateConverter;
import cz.agents.raptor.load.io.imp.ObjectImportException;
import org.junit.Before;
import org.junit.Test;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.OptionalDouble;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class EarliestArrivalProblemTest {
    private Raptor raptor;
    private Timetable timetable;
    private List<Long> results = new ArrayList<>();
    private boolean measure = false;

    @Before
    public void setUp() throws Exception {
        Gtfs gtfs = null;
        try {
            gtfs = Gtfs.parseFolder("src/main/resources/timetables/gtfs/GTFS_20160413/");
        } catch (ObjectImportException e) {
            e.printStackTrace();
        }
        gtfs.removeIllegalElements();
        System.out.println(gtfs);

        timetable = new Timetable(gtfs);
        raptor = new Raptor();
    }

    private EarliestArrivalProblem createEarliestArrivalProblem(String sourceStr, String destStr, String dateStr, String timeStr) {
        Stop sourceStop = timetable.getStopById(sourceStr);
        Stop targetStop = timetable.getStopById(destStr);
        LocalDate date = new GtfsDateConverter().convertToEntityAttribute(dateStr);
        int departureTime = TimeHelper.getTimeInSeconds(timeStr);

        return new EarliestArrivalProblem(timetable, sourceStop, targetStop, date, departureTime);
    }

    private Journey solveProblem(EarliestArrivalProblem eap) {
        long timeStart = System.currentTimeMillis();
        Journey journey = raptor.solve(eap);
        long timeEnd = System.currentTimeMillis();

        if (measure) {
            results.add(timeEnd - timeStart);
        }

//        if (journey != null) {
//            journey.print();
//        }
        return journey;
    }

    private void checkJourney(Journey journey, int tripCount, int transferCount, String departure, String arrival) {
        assertEquals(journey.getTripCount(), tripCount);
        assertEquals(journey.getTransferCount(), transferCount);

        LocalDate startDate = journey.getTimetableStartDate();
        Trip firstTrip = journey.getTrip(0);
        String departureStr = TimeHelper.formatDateTime(startDate, firstTrip.getDepartureAtStop(journey.getPickUpStop(firstTrip)), "dd.MM.yyyy HH:mm:ss");
        assertEquals(departureStr, departure);

        Trip lastTrip = journey.getTrip(tripCount - 1);
        String arrivalStr = TimeHelper.formatDateTime(startDate, lastTrip.getArrivalAtStop(journey.getDropOffStop(lastTrip)), "dd.MM.yyyy HH:mm:ss");
        assertEquals(arrivalStr, arrival);
    }

    // a single bus route on a working day (Stadion Strahov -> Karlovo náměstí)
    private void testSingleLineWorkday() {
        EarliestArrivalProblem eap = createEarliestArrivalProblem("U699Z2", "U237Z9", "20160414", "15:00:00");
        Journey journey = solveProblem(eap);
        checkJourney(journey, 1, 0, "14.04.2016 15:02:00", "14.04.2016 15:16:00");
    }

    // a single bus route on Sunday, should be different from the previous one (Stadion Strahov -> Karlovo náměstí)
    private void testSingleLineHoliday() {
        EarliestArrivalProblem eap = createEarliestArrivalProblem("U699Z2", "U237Z9", "20160417", "15:00:00");
        Journey journey = solveProblem(eap);
        checkJourney(journey, 1, 0, "17.04.2016 15:13:00", "17.04.2016 15:25:00");
    }

    // a single metro route, the first part is a transfer between near stops (Křižíkova -> Můstek - B)
    private void testSingleLineStartTransfer() {
        EarliestArrivalProblem eap = createEarliestArrivalProblem("U758Z2", "U1072Z121", "20160416", "07:00:00");
        Journey journey = solveProblem(eap);
        checkJourney(journey, 1, 1, "16.04.2016 07:10:00", "16.04.2016 07:14:15");
    }

    // a single metro route, the last part is a transfer between near stops (Pražského povstání -> I. P. Pavlova)
    private void testSingleLineEndTransfer() {
        EarliestArrivalProblem eap = createEarliestArrivalProblem("U597Z101", "U190Z1", "20160416", "20:00:00");
        Journey journey = solveProblem(eap);
        checkJourney(journey, 1, 1, "16.04.2016 20:00:15", "16.04.2016 20:03:30");
    }

    // 4 combined routes with 4 transfers at night (Zličín -> Černý Most)
    private void testMultipleLines() {
        EarliestArrivalProblem eap = createEarliestArrivalProblem("U1141Z5", "U897Z1", "20160417", "01:00:00");
        Journey journey = solveProblem(eap);
        checkJourney(journey, 4, 4, "17.04.2016 01:42:00", "17.04.2016 03:13:00");
    }

    // 2 bus routes with a single transfer over midnight (Dejvická -> Holečkova)
    private void testMultipleLinesOverMidnight() {
        EarliestArrivalProblem eap = createEarliestArrivalProblem("U321Z13", "U148Z2", "20160415", "23:50:00");
        Journey journey = solveProblem(eap);
        checkJourney(journey, 2, 1, "15.04.2016 23:51:00", "16.04.2016 00:09:00");
    }

    // 2 combined routes (bus + tram) with no transfer between stops (Koleje Strahov -> Hradčanská)
    private void testMultipleLinesNoTransfer() {
        EarliestArrivalProblem eap = createEarliestArrivalProblem("U981Z1", "U163Z1", "20160415", "08:05:00");
        Journey journey = solveProblem(eap);
        checkJourney(journey, 2, 0, "15.04.2016 08:05:00", "15.04.2016 08:15:00");
    }

    // a date out of schedule, no line should be found
    private void testDateOutOfSchedule() {
        EarliestArrivalProblem eap = createEarliestArrivalProblem("U321Z18", "U237Z102", "20170101", "00:00:00");
        Journey journey = solveProblem(eap);
        assertNull(journey);
    }

    private void testSet() {
        testSingleLineWorkday();

        testSingleLineHoliday();

        testSingleLineStartTransfer();

        testSingleLineEndTransfer();

        testMultipleLines();

        testMultipleLinesOverMidnight();

        testMultipleLinesNoTransfer();

        testDateOutOfSchedule();
    }

    @Test
    public void solve() throws Exception {
        final int DUMMY_TEST_COUNT = 3;
        final int TEST_COUNT = 25;

        for (int i = 0; i < DUMMY_TEST_COUNT; i++) {
            testSet();
        }

        measure = true;
        for (int i = 0; i < TEST_COUNT; i++) {
            testSet();
        }

        Collections.sort(results);

        OptionalDouble avg = results.stream().mapToLong(Long::longValue).average();

        System.out.println(TEST_COUNT + " rounds of tests passed.");
        System.out.println("Min: " + results.get(0) + " ms");
        System.out.println("Max: " + results.get(results.size()-1)  + " ms");
        System.out.println("Med: " + results.get(results.size() / 2) + " ms");
        System.out.println("Avg: " + avg.getAsDouble() + " ms");
    }
}