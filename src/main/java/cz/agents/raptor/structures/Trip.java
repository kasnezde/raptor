package cz.agents.raptor.structures;

import cz.agents.raptor.load.gtfs.GtfsStopTimes;
import cz.agents.raptor.load.gtfs.GtfsTrips;

import java.util.List;
import java.util.stream.Collectors;

/**
 * This class represents a single instance of trip in space-time (the trip can occur only once in a timetable).
 * A trip is associated with a GTFS trip and GTFS stop times. On top, a trip is associated with a particular date.
 */
public class Trip {
    private Route route;
    private GtfsTrips gtfsTrip;
    private List<GtfsStopTimes> gtfsStopTimes;
    private int tripDateSeconds; // date of the trip (midnight) in seconds

    public Trip(GtfsTrips gtfsTrip, List<GtfsStopTimes> gtfsStopTimes, int tripDateSeconds, Route route) {
        this.gtfsTrip = gtfsTrip;
        this.gtfsStopTimes = gtfsStopTimes;
        this.tripDateSeconds = tripDateSeconds;
        this.route = route;
    }

    /**
     * Extracts stop IDs from a list of GtfsStopTimes
     *
     * @param gtfsStopTimes stop times loaded from GTFS file
     * @return a list of stop IDs
     */
    public static List<String> extractStopIds(List<GtfsStopTimes> gtfsStopTimes) {
        return gtfsStopTimes.stream()
                .map(GtfsStopTimes::getStopId)
                .collect(Collectors.toList());
    }

    /**
     * Returns the arrival time of the trip at index-th stop in seconds.
     *
     * @param index index of the stop on the trip
     * @return the time that the trip arrives to the stop in seconds
     */
    public int getArrivalAtStop(int index) {
        return tripDateSeconds + gtfsStopTimes.get(index).getArrivalTime();
    }

    /**
     * Returns the departure time of the trip at index-th stop in seconds.
     *
     * @param index index of the stop on the trip
     * @return the time that the trip arrives to the stop in seconds
     */
    public int getDepartureAtStop(int index) {
        return tripDateSeconds + gtfsStopTimes.get(index).getDepartureTime();
    }

    /**
     * Returns the departure time of the trip at stop in seconds.
     *
     * @param stop index of the stop on the trip
     * @return the time that the trip arrives to the stop in seconds
     */
    public int getDepartureAtStop(Stop stop) {
        return tripDateSeconds + gtfsStopTimes.get(getStopIndex(stop)).getDepartureTime();
    }

    /**
     * Returns the departure time of the trip at stop in seconds.
     *
     * @param stop index of the stop on the trip
     * @return the time that the trip arrives to the stop in seconds
     */
    public int getArrivalAtStop(Stop stop) {
        return tripDateSeconds + gtfsStopTimes.get(getStopIndex(stop)).getArrivalTime();
    }

    /**
     * Returns the order of the given stop on the trip.
     *
     * @param stop a stop on the trip
     * @return the order of the stop on the trip
     */
    private int getStopIndex(Stop stop) {
        for (int i = 0; i < gtfsStopTimes.size(); i++) {
            GtfsStopTimes stopTime = gtfsStopTimes.get(i);
            if (stopTime.getStopId().equals(stop.getId())) {
                return i;
            }
        }
        throw new IllegalStateException("Stop " + stop.getId() + " does not exist in this trip.");
    }

    public Route getRoute() {
        return route;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Trip trip = (Trip) o;

        String routeId = this.getRoute().getId();
        String otherRouteId = trip.getRoute().getId();

        List<String> stopIds = Trip.extractStopIds(gtfsStopTimes);
        List<String> otherStopIds = Trip.extractStopIds(trip.gtfsStopTimes);

        return routeId.equals(otherRouteId) && stopIds.equals(otherStopIds);
    }

    @Override
    public int hashCode() {
        List<String> stopIds = Trip.extractStopIds(gtfsStopTimes);
        return stopIds.hashCode();
    }
}
