package cz.agents.raptor.structures;

import java.util.ArrayList;
import java.util.List;

/**
 * This class represents a route, i.e. a unique set of stop IDs and GTFS route ID.
 */
public class Route {
    private String id;          // unique id of the route
    private String routeId;     // id of the associated GTFS route
    private List<Stop> routeStops = new ArrayList<>();
    private List<Trip> routeTrips = new ArrayList<>();
    private String routeShortName;
    private String routeLongName;

    public Route(String routeId, String routeShortName, String routeLongName) {
        this.routeId = routeId;
        this.routeShortName = routeShortName;
        this.routeLongName = routeLongName;
    }

    public String getId() {
        return id;
    }

    public String getRouteId() {
        return routeId;
    }

    public String getRouteShortName() {
        return routeShortName;
    }

    public String getRouteLongName() {
        return routeLongName;
    }

    public List<Stop> getStops() {
        return routeStops;
    }

    public List<Trip> getTrips() {
        return routeTrips;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void addStop(Stop stop) {
        routeStops.add(stop);
    }

    public void addTrips(List<Trip> trips) {
        routeTrips.addAll(trips);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Route route = (Route) o;

        return id.equals(route.id);
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }
}
