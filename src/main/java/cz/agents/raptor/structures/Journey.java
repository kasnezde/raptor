package cz.agents.raptor.structures;

import cz.agents.raptor.tools.TimeHelper;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * This class represents a result, i.e. a set of trips and transfers it takes to arrive to the destination.
 */
public class Journey {
    private LocalDate timetableStartDate;                 // a base date to count timestamps from
    private List<Trip> trips = new ArrayList<>();
    private List<Transfer> transfers = new ArrayList<>();
    private Map<Trip, Stop> pickUpStops = new HashMap<>();
    private Map<Trip, Stop> dropOffStops = new HashMap<>();

    public LocalDate getTimetableStartDate() {
        return timetableStartDate;
    }

    public Trip getTrip(int index) {
        return trips.get(getTripCount() - index - 1);     // trips are sorted in reversed order
    }

    public Transfer getTransfer(int index) {
        return transfers.get(getTripCount() - index - 1); // transfers are sorted in reversed order
    }

    public int getTripCount() {
        return trips.size();
    }    // assuming no trips are null

    public int getTransferCount() {                       // excluding the null transfers
        int count = 0;
        for (Transfer transfer : transfers) {
            if (transfer != null) {
                count++;
            }
        }
        return count;
    }

    public Stop getPickUpStop(Trip trip) {
        if (pickUpStops.containsKey(trip)) {
            return pickUpStops.get(trip);
        }
        return null;
    }
    public Stop getDropOffStop(Trip trip) {
        if (dropOffStops.containsKey(trip)) {
            return dropOffStops.get(trip);
        }
        return null;
    }

    public void setPickUpStop(Trip trip, Stop stop) {
        pickUpStops.put(trip, stop);
    }

    public void setDropOffStop(Trip trip, Stop stop) {
        dropOffStops.put(trip, stop);
    }

    public void setTimetableStartDate(LocalDate timetableStartDate) {
        this.timetableStartDate = timetableStartDate;
    }

    public void addTrip(Trip trip) {
        trips.add(trip);
    }

    public void addTransfer(Transfer transfer) {
        transfers.add(transfer);
    }

    private void printTransfer(Transfer transfer) {
        System.out.println("---------------");
        System.out.println("TRANSFER");
        System.out.println("transfer " + transfer.getTransferTime() + " seconds from "
                + transfer.getSourceStop().getId() + " to " + transfer.getTargetStop().getId());
    }

    private void printTrip(Trip trip) {

        Stop pickUpStop = this.getPickUpStop(trip);
        Stop dropOffStop = this.getDropOffStop(trip);

        System.out.println("TRIP");
        System.out.println("Route " + trip.getRoute().getRouteShortName());
        System.out.println("depart from " + pickUpStop.getName() + " (" + pickUpStop.getId() + ") at "
                + TimeHelper.formatDateTime(timetableStartDate, trip.getDepartureAtStop(pickUpStop), "dd.MM.yyyy HH:mm:ss"));
        System.out.println("arrive at " + dropOffStop.getName() + " (" + dropOffStop.getId() + ") at "
                + TimeHelper.formatDateTime(timetableStartDate, trip.getArrivalAtStop(dropOffStop), "dd.MM.yyyy HH:mm:ss"));
    }

    /**
     * Prints the representation of the journey to the standard output
     */
    public void print() {
        System.out.println("\n\nJourney:");

        Transfer transfer = this.getTransfer(-1);

        if (transfer != null) {
            printTransfer(transfer);
        }
        for (int i = 0; i < getTripCount(); i++) {
            System.out.println("===================");
            System.out.println(i + 1);

            Trip trip = this.getTrip(i);
            printTrip(trip);

            transfer = this.getTransfer(i);

            if (transfer != null) {
                printTransfer(transfer);
            }
        }
        System.out.println("===================");
    }
}
