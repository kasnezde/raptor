package cz.agents.raptor.structures;

/**
 * This class represents a pair of two stops that are reachable on foot.
 */
public class Transfer {
    private int transferTime;
    private Stop sourceStop;
    private Stop targetStop;

    public Transfer(Stop sourceStop, Stop targetStop, int transferTime) {
        this.sourceStop = sourceStop;
        this.targetStop = targetStop;
        this.transferTime = transferTime;
    }

    public int getTransferTime() {
        return transferTime;
    }

    public Stop getSourceStop() {
        return sourceStop;
    }

    public Stop getTargetStop() {
        return targetStop;
    }
}
