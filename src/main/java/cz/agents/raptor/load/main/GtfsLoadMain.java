package cz.agents.raptor.load.main;

import cz.agents.raptor.load.gtfs.Gtfs;
import cz.agents.raptor.load.io.imp.ObjectImportException;
import org.apache.log4j.xml.DOMConfigurator;

/**
 * @author Marek Cuchý
 */
public class GtfsLoadMain {

	public static void main(String[] args) throws ObjectImportException {
		DOMConfigurator.configure("log4j.xml");

//		String dir = "data/GTFS_20160413/";
		Gtfs folderGtfs = Gtfs.parseFolder("data/GTFS_20160413/");
		System.out.println(folderGtfs);
		Gtfs zipGtfs = Gtfs.parseZip("data/GTFS_20160413/GTFS_20160413.zip");
		System.out.println(zipGtfs);
	}
}
