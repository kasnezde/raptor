package cz.agents.raptor.load.io.reflection;

import java.lang.reflect.AccessibleObject;

/**
 *
 *@author Marek Cuchy
 *
 */
public abstract class ReflectionSourceDescriptor<T extends AccessibleObject> {

	public final Class<?> clazz;
	public final String name;

	public ReflectionSourceDescriptor(Class<?> clazz, String name) {
		super();
		this.clazz = clazz;
		this.name = name;
	}

	public abstract T getReflectionObject() throws ReflectiveOperationException;
	
	public abstract Class<?> getReflectionValueType() throws ReflectiveOperationException;
		
}
