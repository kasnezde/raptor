package cz.agents.raptor.load.io.source;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Marek Cuchy
 */
public abstract class AttributeSource {

	private AttributeSource parent;

	/**
	 * Name used in output (database...).
	 */
	private final String outputName;

	/**
	 * Class according to which the value will be saved.
	 */
	private final Class<?> correspondingClass;

	public AttributeSource(AttributeSource parent) {
		this(parent, null, null);
	}

	public AttributeSource(AttributeSource parent, String outputName, Class<?> correspondingClass) {
		super();
		this.parent = parent;
		this.outputName = outputName;
		this.correspondingClass = correspondingClass;
	}

	/**
	 * Gather value from the source via reflection Method or Field.
	 *
	 * @param source
	 * 		instance from which the value will be gathered.
	 *
	 * @return
	 *
	 * @throws IllegalArgumentException
	 * @throws ReflectiveOperationException
	 */
	protected abstract Object getValueFromSource(
			Object source) throws IllegalArgumentException, ReflectiveOperationException;

	/**
	 * Returns reflection value for the root object in tree structure. <br> WARNING: Not tested for depth greater than
	 * 1!
	 *
	 * @param source
	 * 		- root object in the structure.
	 *
	 * @return
	 *
	 * @throws IllegalArgumentException
	 * @throws IllegalAccessException
	 * @throws InvocationTargetException
	 */
	@SuppressWarnings("unchecked")
	public <T> T getValue(Object source) throws IllegalArgumentException, ReflectiveOperationException {
		List<AttributeSource> pathToRoot = getPathToRoot();

		for (AttributeSource af : pathToRoot) {
			if (source == null) return null;
			source = af.getValueFromSource(source);
		}
		return (T) source;
	}

	public List<AttributeSource> getPathToRoot() {
		if (isRoot()) {
			List<AttributeSource> list = new ArrayList<AttributeSource>();
			list.add(this);
			return list;
		} else {
			List<AttributeSource> list = parent.getPathToRoot();
			list.add(this);
			return list;
		}
	}

	public boolean isRoot() {
		return parent == null;
	}

	public boolean isWrapper() {
		return correspondingClass == null;
	}

	/**
	 * Get class of objects that can be extracted by the attribute source.
	 *
	 * @return
	 */
	public Class<?> getType() {
		return correspondingClass;
	}

	public String getOutputName() {
		return outputName;
	}

	public void setParent(AttributeSource parent) {
		this.parent = parent;
	}

	@Override
	public String toString() {
		return "cz.agents.raptor.load.io.source.AttributeSource [superiorComponent=" + parent + ", outputName=" + outputName + ", " +
			   "correspondingClass=" +
			   correspondingClass + "]";
	}
}
