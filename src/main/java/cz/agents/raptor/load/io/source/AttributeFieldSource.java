package cz.agents.raptor.load.io.source;

import cz.agents.raptor.load.io.reflection.FieldDescriptor;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;

/**
 * Holds {@code Field} and superiorComponent and children in the reflection structure.<br/> Provides method for
 * gathering value from the field.
 *
 * @author Marek Cuchy
 */
public class AttributeFieldSource extends AttributeSource {

	private final Field field;

	/**
	 * @param parent
	 * 		in reflection structure, if this instance is root the superiorComponent should be {@code null}
	 * @param outputName
	 * 		name used for this attribute if the field is used as an output (e.g. column name in database)
	 * @param correspondingClass
	 * @param field
	 */
	public AttributeFieldSource(AttributeSource parent, String outputName, Class<?> correspondingClass, Field field) {
		super(parent, outputName, correspondingClass);
		this.field = field;
		this.field.setAccessible(true);
	}

	public AttributeFieldSource(AttributeSource parent, String name, Class<?> correspondingClass,
								FieldDescriptor fieldDescriptor) throws ReflectiveOperationException {
		this(parent, name, correspondingClass, fieldDescriptor.getReflectionObject());

	}

	@Override
	protected Object getValueFromSource(Object source) throws IllegalArgumentException, IllegalAccessException {
		return this.field.get(source);
	}

	/**
	 * Set the value of the field represented by this {@link AttributeFieldSource}.
	 *
	 * @param object
	 * 		the object of which the field is set
	 * @param value
	 * 		the value that will be set to the field
	 *
	 * @throws IllegalArgumentException
	 * @throws IllegalAccessException
	 */
	public void setValue(Object object, Object value) throws IllegalArgumentException, IllegalAccessException {
		field.set(object, value);
	}

	public boolean isAnnotated(Class<? extends Annotation> annotationClass) {
		return field.isAnnotationPresent(annotationClass);
	}

	public <T extends Annotation> T getAnnotation(Class<T> annotationClass){
		return field.getAnnotation(annotationClass);
	}
}
