package cz.agents.raptor.load.io.source;

import cz.agents.raptor.load.io.reflection.FieldDescriptor;
import cz.agents.raptor.load.io.reflection.MethodDescriptor;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * @author Marek Cuchy
 */
public class AttributeSourceFactory {

	public List<AttributeFieldSource> createAttributeFieldSourcesOnlyNonStatic(Class<?> clazz) throws
																							   ReflectiveOperationException {
		List<AttributeFieldSource> sources = new ArrayList<>();

		for (Field f : clazz.getDeclaredFields()) {
			if (Modifier.isStatic(f.getModifiers())) continue;
			sources.add(createAttributeFieldSource(clazz, f.getName(), f.getName()));
		}
		return sources;
	}

	public List<AttributeFieldSource> createAttributeFieldSources(Class<?> clazz) throws ReflectiveOperationException {
		List<AttributeFieldSource> sources = new ArrayList<>();

		for (Field f : clazz.getDeclaredFields()) {
			sources.add(createAttributeFieldSource(clazz, f.getName(), f.getName()));
		}
		return sources;
	}

	public List<AttributeFieldSource> createAttributeFieldSources(Class<?> clazz, Set<String> allowedFields) throws
																											 ReflectiveOperationException {
		List<AttributeFieldSource> sources = new ArrayList<>();

		for (Field f : clazz.getDeclaredFields()) {
			if (allowedFields.contains(f.getName())) {
				sources.add(createAttributeFieldSource(clazz, f.getName(), f.getName()));
			}
		}
		return sources;
	}

	public AttributeFieldSource createAttributeFieldSource(Class<?> clazz, String fieldName, String outputName) throws
																												ReflectiveOperationException {
		FieldDescriptor fieldDescriptor = new FieldDescriptor(clazz, fieldName);
		return new AttributeFieldSource(null, outputName, fieldDescriptor.getReflectionValueType(), fieldDescriptor);

	}

	public AttributeFieldSource createAttributeFieldSource(Class<?> clazz, String fieldName, String outputName,
														   AttributeSource parent) throws
																				   ReflectiveOperationException {
		FieldDescriptor fieldDescriptor = new FieldDescriptor(clazz, fieldName);
		return new AttributeFieldSource(parent, outputName, fieldDescriptor.getReflectionValueType(), fieldDescriptor);

	}

	public AttributeFieldSource createAttributeFieldSource(Class<?> clazz, String fieldName) throws
																							 ReflectiveOperationException {
		return createAttributeFieldSource(clazz, fieldName, null, null, null);
	}

	public AttributeFieldSource createAttributeFieldSource(Class<?> clazz, String fieldName,
														   AttributeSource parent) throws
																				   ReflectiveOperationException {
		return createAttributeFieldSource(clazz, fieldName, null, parent, null);
	}

	public AttributeFieldSource createAttributeFieldSource(Class<?> clazz, String fieldName, String outputName,
														   Class<?> correspondingClass) throws
																						ReflectiveOperationException {
		return createAttributeFieldSource(clazz, fieldName, outputName, null, correspondingClass);
	}

	public AttributeFieldSource createAttributeFieldSource(Class<?> clazz, String fieldName, String outputName,
														   AttributeSource parent, Class<?> columnType) throws
																										ReflectiveOperationException {
		FieldDescriptor fieldDescriptor = new FieldDescriptor(clazz, fieldName);
		return new AttributeFieldSource(parent, outputName, columnType, fieldDescriptor);
	}

	public AttributeMethodSource createAttributeMethodSource(Class<?> clazz, String methodName, String outputName,
															 AttributeSource parent) throws
																					 ReflectiveOperationException {
		MethodDescriptor methodDescriptor = new MethodDescriptor(clazz, methodName);
		Class<?> correspondingClass = methodDescriptor.getReflectionObject().getReturnType();
		return new AttributeMethodSource(parent, outputName, correspondingClass, methodDescriptor);
	}

	public AttributeMethodSource createAttributeMethodSource(Class<?> clazz, String methodName, String outputName,
															 AttributeSource parent, Class<?> correspondingClass)
			throws
																												  ReflectiveOperationException {
		MethodDescriptor methodDescriptor = new MethodDescriptor(clazz, methodName);
		return new AttributeMethodSource(parent, outputName, correspondingClass, methodDescriptor);
	}
}
