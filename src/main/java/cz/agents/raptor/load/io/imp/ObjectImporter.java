package cz.agents.raptor.load.io.imp;

import cz.agents.raptor.load.io.source.AttributeFieldSource;
import org.apache.log4j.Logger;

import java.lang.reflect.Constructor;
import java.util.*;
import java.util.Map.Entry;
import java.util.function.Function;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import static java.util.stream.Collectors.toMap;

/**
 * Abstract class of object importers from different sources. Although it support the
 *
 * @author Marek Cuchý
 */
public abstract class ObjectImporter<T> implements Iterator<T> {

	private static final Logger LOGGER = Logger.getLogger(ObjectImporter.class);

	public static final ExceptionPolicy DEFAULT_EXCEPTION_POLICY = ExceptionPolicy.THROW;

	private final Map<String, AttributeFieldSource> fields;
	private final ExceptionPolicy exceptionPolicy;
	private final Constructor<T> constructor;

	private ObjectImportAttributes next;

	public ObjectImporter(List<AttributeFieldSource> fields, Class<T> dataClass,
						  ExceptionPolicy exceptionPolicy) throws ObjectImportException {
		this.fields = fields.stream().collect(toMap(AttributeFieldSource::getOutputName, Function.identity()));
		this.exceptionPolicy = exceptionPolicy;
		try {
			constructor = dataClass.getDeclaredConstructor();
			constructor.setAccessible(true);
		} catch (NoSuchMethodException e) {
			throw new ObjectImportException(e);
		}
	}

	public List<T> importAll() throws ObjectImportException {
		List<T> data = new ArrayList<>();
		try {
			this.forEachRemaining(data::add);
		} catch (IllegalStateException e) {
			throw new ObjectImportException(e);
		}
		return data;
	}

	/**
	 * It creates a stream of imported objects but once a terminal operation on the stream is executed the importer
	 * will
	 * appear as empty.
	 *
	 * @return
	 */
	public Stream<T> stream() {
		return StreamSupport.stream(
				Spliterators.spliteratorUnknownSize(this, Spliterator.IMMUTABLE | Spliterator.ORDERED), false);
	}

	@Override
	public final boolean hasNext() {
		if (next != null) return true;
		if (!sourceHasNext()) {
			return false;
		} else {
			while (sourceHasNext() && next == null) {
				next = readAttributes();
				if (!next.complete() && exceptionPolicy == ExceptionPolicy.SKIP) {
//					LOGGER.warn("Record skipped. Exceptions thrown for fields: " + next.exceptions.keySet());
					next = null;
				}
			}
			return next != null;
		}
	}

	@Override
	public final T next() {
		if (hasNext()) {
			if (!next.complete() && exceptionPolicy == ExceptionPolicy.THROW) {
				throw new IllegalStateException("Exceptions thrown for fields: " + next.exceptions.keySet(),
												next.exceptions.values().iterator().next());
			}
			try {
				T instance = constructor.newInstance();
//
//				for (Entry<String, Object> entry : next) {
//					String name = entry.getKey();
//					cz.agents.raptor.load.io.source.AttributeFieldSource field = fields.get(name);
//					if (next.contains(name)) {
//						field.setValue(instance, next.get(name));
//					} else if (exceptionPolicy == ExceptionPolicy.NULL) {
//						if (field.getType().isPrimitive())
//							throw new UnsupportedOperationException("Field " + name + " can't be set to null.");
//						field.setValue(instance, null);
//					} else{
//						throw new IllegalStateException("Unreachable.");
//					}
//				}

				for (AttributeFieldSource field : fields.values()) {
					String name = field.getOutputName();
					if (next.contains(name)) {
						field.setValue(instance, next.get(name));
					} else if (exceptionPolicy == ExceptionPolicy.NULL) {
						if (field.getType().isPrimitive())
							throw new UnsupportedOperationException("Field " + name + " can't be set to null.");
						field.setValue(instance, null);
					} else{
						throw new IllegalStateException("Unreachable.");
					}
				}
				next = null;
				return instance;
			} catch (ReflectiveOperationException e) {
				throw new IllegalStateException(e);
			}
		} else {
			throw new NoSuchElementException();
		}
	}

	/**
	 * Reads all attributes for current record. If an exception occurs during processing of an attribute, the exception
	 * is stored in the resulting map instead of anticipated value.
	 *
	 * @return
	 */
	protected abstract ObjectImportAttributes readAttributes();

	protected abstract boolean sourceHasNext();

	public static class ObjectImportAttributes implements Iterable<Entry<String,Object>>{

		private Map<String, Object> attributes = new LinkedHashMap<>();
		private Map<String, Exception> exceptions = new LinkedHashMap<>();

		public boolean contains(String name) {
			return attributes.containsKey(name);
		}

		public Object get(String name) {
			return attributes.get(name);
		}

		public void addAttribute(String name, Object value) {
			attributes.put(name, value);
		}

		public void addException(String name, Exception e) {
			exceptions.put(name, e);
		}

		public boolean complete() {
			return exceptions.isEmpty();
		}

		public boolean isEmpty() {
			return attributes.isEmpty() && exceptions.isEmpty();
		}

		@Override
		public Iterator<Entry<String, Object>> iterator() {
			return attributes.entrySet().iterator();
		}
	}

	/**
	 * Policy of handling exceptions thrown during processing of a record.
	 */
	public enum ExceptionPolicy {
		/**
		 * The exception is rethrown.
		 */
		THROW,
		/**
		 * The record for which an exception is thrown is skipped.
		 */
		SKIP,
		/**
		 * The attributes for which an exception is thrown is set to null for non-primitive types. If the attribute is
		 * of primitive type exception is thrown.
		 */
		NULL
	}
}
