package cz.agents.raptor.load.io.csv;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableMap.Builder;
import com.opencsv.CSVParser;
import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;
import cz.agents.raptor.load.io.ImpExpUtils;
import cz.agents.raptor.load.io.imp.ObjectImportException;
import cz.agents.raptor.load.io.imp.ObjectImporter;
import cz.agents.raptor.load.io.imp.ObjectImporter.ExceptionPolicy;
import cz.agents.raptor.load.io.imp.ObjectImporterFactory;
import cz.agents.raptor.load.io.source.AttributeFieldSource;

import javax.persistence.AttributeConverter;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Method;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;

import static java.util.stream.Collectors.toSet;

/**
 * @author Marek Cuchý
 */
public class ObjectFromCsvImporterFactory extends ObjectImporterFactory {

	private static final Map<Class<?>, Function<String, ? extends Object>> CONVERTERS = new HashMap<>();

	private static final String DEFAULT_LINE_END = "";

	static {
		CONVERTERS.put(Double.class, Double::valueOf);
		CONVERTERS.put(double.class, Double::valueOf);
		CONVERTERS.put(Float.class, Float::valueOf);
		CONVERTERS.put(float.class, Float::valueOf);
		CONVERTERS.put(Long.class, Long::valueOf);
		CONVERTERS.put(long.class, Long::valueOf);
		CONVERTERS.put(Integer.class, Integer::valueOf);
		CONVERTERS.put(int.class, Integer::valueOf);
		CONVERTERS.put(Short.class, Short::valueOf);
		CONVERTERS.put(short.class, Short::valueOf);
		CONVERTERS.put(Byte.class, Byte::valueOf);
		CONVERTERS.put(byte.class, Byte::valueOf);
		CONVERTERS.put(Boolean.class, ObjectFromCsvImporterFactory::parseBoolean);
		CONVERTERS.put(boolean.class, ObjectFromCsvImporterFactory::parseBoolean);
		CONVERTERS.put(Character.class, ObjectFromCsvImporterFactory::parseChar);
		CONVERTERS.put(char.class, ObjectFromCsvImporterFactory::parseChar);
		CONVERTERS.put(String.class, s -> s);
	}

	private final char delimiter;
	private final Charset encoding;
	private final boolean hasHeader;
	private final String lineEnd;

	public ObjectFromCsvImporterFactory() {
		this(CSVWriter.DEFAULT_SEPARATOR, Charset.defaultCharset(), true, DEFAULT_LINE_END);
	}

	public ObjectFromCsvImporterFactory(char delimiter) {
		this(delimiter, Charset.defaultCharset(), true, DEFAULT_LINE_END);
	}

	public ObjectFromCsvImporterFactory(char delimiter, Charset encoding) {
		this(delimiter, encoding, true, DEFAULT_LINE_END);

	}

	public ObjectFromCsvImporterFactory(char delimiter, Charset encoding, boolean hasHeader) {
		this(delimiter, encoding, hasHeader, DEFAULT_LINE_END);
	}

	public ObjectFromCsvImporterFactory(char delimiter, Charset encoding, boolean hasHeader, String lineEnd) {
		this.delimiter = delimiter;
		this.encoding = encoding;
		this.hasHeader = hasHeader;
		this.lineEnd = lineEnd;
	}

	public ObjectFromCsvImporterFactory(String prefix, String postfix) {
		this(prefix, postfix, CSVWriter.DEFAULT_SEPARATOR, Charset.defaultCharset(), true, DEFAULT_LINE_END);
	}

	public ObjectFromCsvImporterFactory(String prefix, String postfix, char delimiter) {
		this(prefix, postfix, delimiter, Charset.defaultCharset(), true, DEFAULT_LINE_END);
	}

	public ObjectFromCsvImporterFactory(String prefix, String postfix, char delimiter, Charset encoding) {
		this(prefix, postfix, delimiter, encoding, true, DEFAULT_LINE_END);
	}

	public ObjectFromCsvImporterFactory(String prefix, String postfix, char delimiter, Charset encoding,
										boolean hasHeader, String lineEnd) {
		super(prefix, postfix);
		this.delimiter = delimiter;
		this.encoding = encoding;
		this.hasHeader = hasHeader;
		this.lineEnd = lineEnd;
	}

	@Override
	public <T> ObjectFromCsvImporter<T> createImporter(Class<T> clazz, String sourceName) throws
																						  ObjectImportException {
		return createImporterFromExactPath(clazz, sourceName + ".cz.agents.raptor.load.io.csv");
	}

	public <T> ObjectFromCsvImporter<T> createFromInputStream(Class<T> clazz, InputStream inputStream,
															  ExceptionPolicy exceptionPolicy) throws
																									   ObjectImportException {
		try {
			List<AttributeFieldSource> sources = ImpExpUtils
					.createAttributeFieldSourceForAllFieldsIncludingSuperClasses(
					clazz);
			CSVReader reader = new CSVReader(new InputStreamReader(inputStream, encoding), delimiter,
											 CSVParser.DEFAULT_QUOTE_CHARACTER);
			String[] header = null;
			if (hasHeader) {
				try {
					header = reader.readNext();
				} catch (IOException e) {
					throw new ObjectImportException(e);
				}
			}
			Map<String, Integer> indexes = createIndexes(sources, header);
			Map<String, Function<String, ?>> converters = createConverters(sources, clazz);

			return new ObjectFromCsvImporter<>(sources, clazz, indexes, converters, reader, lineEnd, exceptionPolicy);
		} catch (ReflectiveOperationException e) {
			throw new ObjectImportException(e);
		}
	}

	public <T> ObjectFromCsvImporter<T> createImporterFromExactPath(Class<T> clazz, String exactPath) throws
																									  ObjectImportException {
		return createImporterFromExactPath(clazz, exactPath, ObjectImporter.DEFAULT_EXCEPTION_POLICY);
	}

	public <T> ObjectFromCsvImporter<T> createImporterFromExactPath(Class<T> clazz, String exactPath,
																	ExceptionPolicy exceptionPolicy) throws
																									 ObjectImportException {
		try {
			return createFromInputStream(clazz, new FileInputStream(exactPath), exceptionPolicy);
		} catch (IOException e) {
			throw new ObjectImportException(e);
		}
	}

	public static Map<String, Function<String, ?>> createConverters(List<AttributeFieldSource> sources,
																	Class<?> exportClazz) throws NoSuchMethodException,
																								 ObjectImportException {
		ImmutableMap.Builder<String, Function<String, ?>> converters = ImmutableMap.builder();
		for (AttributeFieldSource field : sources) {
			Class<?> type = field.getType();
			if (field.isAnnotated(CsvConvert.class)) {
				CsvConvert convert = field.getAnnotation(CsvConvert.class);
				Class<?> converterClass = convert.converter();
				if (AttributeConverter.class.isAssignableFrom(converterClass)) {
					AttributeConverter<?, String> converter;
					try {
						converter = (AttributeConverter<?, String>) converterClass.newInstance();
					} catch (InstantiationException | IllegalAccessException e) {
						throw new ObjectImportException("Converter cannot be created.", e);
					}
					addConverter(converters, field.getOutputName(), converter::convertToEntityAttribute);
				} else {
					throw new ObjectImportException(
							"Defined CSV converter isn't subclass of AttributeConverter: " + converterClass);
				}
			} else if (type.isEnum()) {
				addConverter(converters, field.getOutputName(), ImpExpUtils.createEnumConverter(type));
			} else if (CONVERTERS.containsKey(type)) {
				addConverter(converters, field.getOutputName(), CONVERTERS.get(type));
			} else {
				throw new ObjectImportException("Field " + field.getOutputName() + " is of non-convertible type");
			}
		}
		return converters.build();
	}

	private static Builder<String, Function<String, ?>> addConverter(Builder<String, Function<String, ?>> converters,
																	 String outputName, Function<String, ?> f) {
		return converters.put(outputName, s -> s.isEmpty() ? null : f.apply(s));
	}

	private static Method getMethod(String name, Class<?> declaredClass, Class<?> argumentClass) throws
																								 ObjectImportException {
		Class<?> functionClass = declaredClass;
		while (functionClass != Object.class) {
			try {
				return functionClass.getDeclaredMethod(name, argumentClass);
			} catch (NoSuchMethodException e) {
				functionClass = declaredClass.getSuperclass();
			}
		}
		throw new ObjectImportException("Suitable method with name " + name +
										" doesn't exist in declared class or its superclasses. Declared class = " +
										declaredClass);
	}

	public static Map<String, Integer> createIndexes(List<AttributeFieldSource> sources, String[] header) {
		ImmutableMap.Builder<String, Integer> indexes = ImmutableMap.builder();
		if (header == null) {
			int index = 0;
			for (AttributeFieldSource field : sources) {
				indexes.put(field.getOutputName(), index++);
			}
		} else {
			Set<String> fields = sources.stream().map(AttributeFieldSource::getOutputName).collect(toSet());
			for (int i = 0; i < header.length; i++) {
				String title = header[i];
				if (fields.contains(title)) {
					indexes.put(title, i);
				}
			}
		}
		return indexes.build();
	}

	private static Boolean parseBoolean(String value) {
		if ("t".equalsIgnoreCase(value) || "true".equalsIgnoreCase(value) || "1".equalsIgnoreCase(value))
			return Boolean.TRUE;
		if ("f".equalsIgnoreCase(value) || "false".equalsIgnoreCase(value) || "0".equalsIgnoreCase(value))
			return Boolean.FALSE;
		throw new IllegalArgumentException("Value can't be converted to boolean (t, f, true, false, 1, 0): " + value);
	}

	private static Character parseChar(String value) {
		if (value.length() != 1)
			throw new IllegalArgumentException("Can't be converted to char - length isn't 1: " + value.length());
		return value.charAt(0);
	}
}
