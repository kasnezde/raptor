package cz.agents.raptor.load.io.csv;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Converter used only for conversion to CSV.
 *
 * @author Marek Cuchý
 */
@Target({METHOD, FIELD, TYPE})
@Retention(RUNTIME)
public @interface CsvConvert {

	/**
	 * Specifies the converter to be applied. A value for this
	 * element must be specified if multiple converters would
	 * otherwise apply.
	 */
	Class<?> converter() default void.class;
}
