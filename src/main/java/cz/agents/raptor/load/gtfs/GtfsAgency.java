package cz.agents.raptor.load.gtfs;

import javax.persistence.Id;

/**
 * @author Marek Cuchý
 */
public class GtfsAgency {

	@Id
	public final String agencyId;
	public final String agencyName;
	public final String agencyUrl;
	public final String agencyTimezone;
	public final String agencyLang;
	public final String agencyPhone;
	public final String agencyFareUrl;

	private GtfsAgency() {
		this(null,null,null,null,null,null,null);
	}

	public GtfsAgency(String agencyId, String agencyName, String agencyUrl, String agencyTimezone, String agencyLang,
					  String agencyPhone, String agencyFareUrl) {
		this.agencyId = agencyId;
		this.agencyName = agencyName;
		this.agencyUrl = agencyUrl;
		this.agencyTimezone = agencyTimezone;
		this.agencyLang = agencyLang;
		this.agencyPhone = agencyPhone;
		this.agencyFareUrl = agencyFareUrl;
	}

	public String getAgencyId() {
		return agencyId;
	}

	public String getAgencyName() {
		return agencyName;
	}

	public String getAgencyUrl() {
		return agencyUrl;
	}

	public String getAgencyTimezone() {
		return agencyTimezone;
	}

	public String getAgencyLang() {
		return agencyLang;
	}

	public String getAgencyPhone() {
		return agencyPhone;
	}

	public String getAgencyFareUrl() {
		return agencyFareUrl;
	}

	@Override
	public String toString() {
		return "GtfsAgency [" +
			   "agencyId='" + agencyId + '\'' +
			   ", agencyName='" + agencyName + '\'' +
			   ", agencyUrl='" + agencyUrl + '\'' +
			   ", agencyTimezone='" + agencyTimezone + '\'' +
			   ", agencyLang='" + agencyLang + '\'' +
			   ", agencyPhone='" + agencyPhone + '\'' +
			   ", agencyFareUrl='" + agencyFareUrl + '\'' +
			   ']';
	}
}
