package cz.agents.raptor.load.gtfs;

import javax.persistence.AttributeConverter;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

/**
 * @author Marek Cuchý
 */
public class GtfsDateConverter implements AttributeConverter<LocalDate,String> {

	private static final DateTimeFormatter DATE_FORMAT = DateTimeFormatter.ofPattern("yyyyMMdd");

	public String convertToDatabaseColumn(LocalDate attribute) {
		return attribute.format(DATE_FORMAT);
	}

	public LocalDate convertToEntityAttribute(String dbData) {
		return LocalDate.parse(dbData, DATE_FORMAT);
	}
}
