package cz.agents.raptor.load.gtfs;

import cz.agents.raptor.load.io.csv.CsvConvert;

import javax.persistence.Id;
import java.time.LocalDate;

/**
 * @author Marek Cuchý
 */
public class GtfsCalendarDates {

	@Id
	public final String serviceId;
	@Id
	@CsvConvert(converter = GtfsDateConverter.class)
	public final LocalDate date;
	public final short exceptionType;

	private GtfsCalendarDates() {
		this(null, null, (short) 0);
	}

	public GtfsCalendarDates(String serviceId, LocalDate date, short exceptionType) {
		this.serviceId = serviceId;
		this.date = date;
		this.exceptionType = exceptionType;
	}
}
