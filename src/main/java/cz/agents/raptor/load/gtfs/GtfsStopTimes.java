package cz.agents.raptor.load.gtfs;

import cz.agents.raptor.load.io.csv.CsvConvert;

import javax.persistence.Id;

/**
 * @author Marek Cuchý
 */
public class GtfsStopTimes {
	@Id
	public final String tripId;

    @CsvConvert(converter = GtfsTimeConverter.class)
    private int arrivalTime;
    @CsvConvert(converter = GtfsTimeConverter.class)
    private int departureTime;

	public final String stopId;

	@Id
	public final int stopSequence;
	public final String stopHeadsign;
	public final Short pickupType;
	public final Short dropOffType;
	public final Double shapeDistTraveled;

	private GtfsStopTimes() {
		this(null, 0, 0, null, 0, null, null, null, null);
	}

	public GtfsStopTimes(String tripId, int arrivalTime, int departureTime, String stopId, int stopSequence,
						 String stopHeadsign, Short pickupType, Short dropOffType, Double shapeDistTraveled) {
		this.tripId = tripId;
		this.arrivalTime = arrivalTime;
		this.departureTime = departureTime;
		this.stopId = stopId;
		this.stopSequence = stopSequence;
		this.stopHeadsign = stopHeadsign;
		this.pickupType = pickupType;
		this.dropOffType = dropOffType;
		this.shapeDistTraveled = shapeDistTraveled;
	}

	public String getTripId() {
		return tripId;
	}

	public int getArrivalTime() {
		return arrivalTime;
	}

	public int getDepartureTime() {
		return departureTime;
	}

	public String getStopId() {
		return stopId;
	}

	public int getStopSequence() {
		return stopSequence;
	}

	public String getStopHeadsign() {
		return stopHeadsign;
	}

	public Short getPickupType() {
		return pickupType;
	}

	public Short getDropOffType() {
		return dropOffType;
	}

	public Double getShapeDistTraveled() {
		return shapeDistTraveled;
	}
}
