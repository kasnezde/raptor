package cz.agents.raptor.load.gtfs;

import javax.persistence.Id;

/**
 * @author Marek Cuchý
 */
public class GtfsRoutes {

	@Id
	public final String routeId;
	public final String agencyId;
	public final String routeShortName;
	public final String routeLongName;
	public final String routeDesc;
	public final short routeType;
	public final String url;
	public final String color;
	public final String textColor;

	private GtfsRoutes() {
		this(null,null,null,null,null,(short) 0,null,null,null);
	}

	public GtfsRoutes(String routeId, String agencyId, String routeShortName, String routeLongName, String routeDesc,
					  short routeType, String url, String color, String textColor) {
		this.routeId = routeId;
		this.agencyId = agencyId;
		this.routeShortName = routeShortName;
		this.routeLongName = routeLongName;
		this.routeDesc = routeDesc;
		this.routeType = routeType;
		this.url = url;
		this.color = color;
		this.textColor = textColor;
	}

	public String getRouteId() {
		return routeId;
	}

	public String getAgencyId() {
		return agencyId;
	}

	public String getRouteShortName() {
		return routeShortName;
	}

	public String getRouteLongName() {
		return routeLongName;
	}

	public String getRouteDesc() {
		return routeDesc;
	}

	public short getRouteType() {
		return routeType;
	}

	public String getUrl() {
		return url;
	}

	public String getColor() {
		return color;
	}

	public String getTextColor() {
		return textColor;
	}

	@Override
	public String toString() {
		return "GtfsRoutes [" +
			   "routeId='" + routeId + '\'' +
			   ", agencyId='" + agencyId + '\'' +
			   ", routeShortName='" + routeShortName + '\'' +
			   ", routeLongName='" + routeLongName + '\'' +
			   ", routeDesc='" + routeDesc + '\'' +
			   ", routeType=" + routeType +
			   ", url='" + url + '\'' +
			   ", color='" + color + '\'' +
			   ", textColor='" + textColor + '\'' +
			   ']';
	}
}
