package cz.agents.raptor.load.gtfs;

import javax.persistence.Id;

/**
 * @author Marek Cuchý
 */
public class GtfsStops {

	@Id
	public final String stopId;
	public final String stopCode;
	public final String stopName;
	public final String stopDesc;
	public final double stopLat;
	public final double stopLon;
	public final String zoneId;
	public final String stopUrl;
	public final Boolean locationType;
	public final String parentStation;
	public final String stopTimezone;
	public final Short wheelchairBoarding;

	private GtfsStops() {
		this(null, null, null, null, 0, 0, null, null, null, null, null, null);

	}

	public GtfsStops(String stopId, String stopCode, String stopName, String stopDesc, double stopLat, double stopLon,
					 String zoneId, String stopUrl, Boolean locationType, String parentStation, String stopTimezone,
					 Short wheelchairBoarding) {
		this.stopId = stopId;
		this.stopCode = stopCode;
		this.stopName = stopName;
		this.stopDesc = stopDesc;
		this.stopLat = stopLat;
		this.stopLon = stopLon;
		this.zoneId = zoneId;
		this.stopUrl = stopUrl;
		this.locationType = locationType;
		this.parentStation = parentStation;
		this.stopTimezone = stopTimezone;
		this.wheelchairBoarding = wheelchairBoarding;
	}

}
