package cz.agents.raptor.load.gtfs;

import javax.persistence.Id;

/**
 * @author Marek Cuchý
 */
public class GtfsTrips {

	public final String routeId;
	public final String serviceId;
	@Id
	public final String tripId;
	public final String tripHeadsign;
	public final String tripShortName;
	public final Boolean directionId;
	public final String blockId;
	public final String shapeId;
	public final Short wheelchairAccessible;
	public final Short bikesAllowed;

	private GtfsTrips() {
		this(null,null,null,null,null,null,null,null,null,null);
	}

	public GtfsTrips(String routeId, String serviceId, String tripId, String tripHeadsign, String tripShortName,
					 Boolean directionId, String blockId, String shapeId, Short wheelchairAccessible,
					 Short bikesAllowed) {
		this.routeId = routeId;
		this.serviceId = serviceId;
		this.tripId = tripId;
		this.tripHeadsign = tripHeadsign;
		this.tripShortName = tripShortName;
		this.directionId = directionId;
		this.blockId = blockId;
		this.shapeId = shapeId;
		this.wheelchairAccessible = wheelchairAccessible;
		this.bikesAllowed = bikesAllowed;
	}

	public String getRouteId() {
		return routeId;
	}

	public String getServiceId() {
		return serviceId;
	}

	public String getTripId() {
		return tripId;
	}

	public String getTripHeadsign() {
		return tripHeadsign;
	}

	public String getTripShortName() {
		return tripShortName;
	}

	public Boolean getDirectionId() {
		return directionId;
	}

	public String getBlockId() {
		return blockId;
	}

	public String getShapeId() {
		return shapeId;
	}
}
