# RAPTOR

## Introduction
The library implements the **RAPTOR (Round-bAsed Public Transit Optimized Router)** routing algorithm on public transit data in GTFS format. The library uses Java 8.

## Algorithm
The algorithm has been proposed by Delling D. et al. in [1]. For two given stops, it computes all Pareto-optimal journeys minimizing the **arrival time** and the **number of transfers** made between them. 

RAPTOR operates in **rounds**, one per transfer. It computes arrival times by traversing every route (such as a bus line) at most once per round. The total running time the algorithm is linear per round. In total, solving the earliest arrival problem takes *O(K(|r|+|T|+|F|)*, where *K* is a number of rounds, *r* is the set of routes, *T* is the set of trips and *F* is the set of transfers.

## Input
The input of the algorithm is in the [General Transit Feed Specification (GTFS)](https://developers.google.com/transit/gtfs/) format. The GTFS format defines a common format for public transportation schedules and associated geographic information.

An example is the current schedule of public transport in Prague. It can be downloaded at the [Prague OpenData portal](http://opendata.praha.eu/dataset/dpp-jizdni-rady).

## Usage

### Library interface
1) Load a timetable in GTFS format

- using a ZIP archive

```java
import cz.agents.raptor.load.gtfs.Gtfs;
gtfs = Gtfs.parseZip("path_to_gtfs_zip_file");
```

- or using a folder

```java
import cz.agents.raptor.load.gtfs.Gtfs;
gtfs = Gtfs.parseFolder("path_to_folder_with_gtfs_files");
```

The ZIP archive (or folder) contains data in GTFS format (see the section **Input**).

2) Construct a timetable for RAPTOR
```java
import cz.agents.raptor.structures.Timetable;
Timetable timetable = new Timetable(gtfs);
```

3) Create an instance of an earliest arrival problem
```java
import cz.agents.raptor.load.gtfs.GtfsDateConverter;
import cz.agents.raptor.structures.Stop;
import cz.agents.raptor.problems.EarliestArrivalProblem;
import cz.agents.raptor.tools.TimeHelper;
import java.time.LocalDate;

Stop sourceStop = timetable.getStopById("source_stop_id");
Stop targetStop = timetable.getStopById("destination_stop_id");
LocalDate departureDate = LocalDate.parse("date_as_yyyyMMdd", "yyyyMMdd");
int departureTime = TimeHelper.parseTime("time_as_HH:mm:ss");

EarliestArrivalProblem eap = new EarliestArrivalProblem(timetable, sourceStop, targetStop, departureDate, departureTime);
```

4) Use RAPTOR to solve the problem
```java
Raptor raptor = new Raptor();
Journey journey = raptor.solve(eap);
```

5) Print the result
```java
if (journey != null) {
    journey.print();
} else {
    System.out.println("Journey not found.");
}
```

6) Alternatively, use the interface of Journey to process the result.
```java
int numberOfTrips = journey.getTripCount();
int numberOfTransfers = journey.getTransferCount();

Trip lastTrip = journey.getTrip(numberOfTrips - 1);
Stop lastStop = journey.getDropOffStop(trip);

LocalDate timetableStartDate = journey.getTimetableStartDate();
int arrivalTimeInSeconds = lastTrip.getArrivalAtStop(lastStop);
String arrivalDateTime = TimeHelper.formatDateTime(timetableStartDate, arrivalTimeInSeconds, "dd.MM.yyyy HH:mm:ss");

System.out.println("The earliest trip arrives at " + arrivalDateTime);
```

### Stand-alone program
1) Run `cz.agents.raptor.Main.java`

2) Enter path to the folder with GTFS timetable *(loading can take some time...)*.

3) Enter source stop id, destination stop id, departure date and departure time.

4) Result will be printed to the standard output.

5) Go to 3).

## Notes

- Transfers between stops are calculated **automatically** based on the geographical coordinates of each stop. The limit is `Timetable.MAX_TRANSFER_DISTANCE` which is set to 500 meters by default.
- Arrival and departure times are represented as seconds from the base date (defined as the start date of the earliest service in timetable). To represent the arrival / departure time as a regular date-time, use `TimeHelper.getDateTime(timetableStartDate, time)`.

## References
[1] DELLING, Daniel; PAJOR, Thomas; WERNECK, Renato F. Round-based public transit routing. Transportation Science, 2014, 49.3: 591-604.